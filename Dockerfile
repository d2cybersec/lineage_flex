FROM rocker/r-ver:latest

RUN apt-get update && apt-get install -y \
git-core \
libcurl4-openssl-dev \
libgit2-dev \
libicu-dev \
libssl-dev \
libxml2-dev \
pandoc \
pandoc-citeproc \
&& rm -rf /var/lib/apt/lists/*

RUN R -e "install.packages(c('dplyr','flexdashboard','DT','knitr','rmarkdown','xml2','rvest','lubridate','stringr'),repos='http://cran.rstudio.com')"
